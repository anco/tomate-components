import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { LineData, Stop } from '../../types.ts/line';

/**
 * Element that holds a bus line information.
 */
@customElement('tomate-line')
export class TomateLine extends LitElement {
  static override styles = css`
    .line-container .line-prefix {
      color: tomato;
      font-size: large;
    }

    .line-container .line-prefix>span {
      font-weight: var(--sl-font-weight-semibold);
      font-family: var(--sl-font-mono);
      padding-left: var(--sl-spacing-2x-small);
    }

    .line-container .line-suffix {
      font-size: smaller;
    }

    sl-button.line-button::part(base) {
      border-style: none;
      justify-content: start;
      align-items: center;
    }

    sl-button.line-button::part(label) {
      white-space: normal;
      line-height: var(--sl-line-height-denser)
    }

    sl-button.line-button::part(suffix) {
      flex-grow: 1;
      justify-content: end;
    }
  `;

  /**
   * Line Prefix. Usually 2 or 3 capital letters.
   */
  @property()
  linePrefix = 'AA';

  /**
   * Prefix Icon. Placed before `linePrefix`.
   */
  @property()
  prefixIcon = 'bus-front-fill';

  /**
   * Line Suffix. Usually IDA or VUELTA.
   */
  @property()
  lineSuffix = 'IDA';

  /**
   * Route Direction. I or V.
   */
  @property()
  direction = 'I'

  @property()
  _clickCallback = () => { }

  /**
   * Leaving this here because I think it should work
   * given how it's used but it seems to get optimized away when compiling
   * (see usage in render())
   */
  @property()
  loading = true

  /** Extra data */
  lineData: LineData | null = null;
  routeName: string | null = null;
  routeId: string | null = null;
  stopsData: Stop[] | null = null;
  stopsCoordinates: number[][] = [];


  override render() {
    return html`
      <div class="line-container">
        <sl-button @click="${this._clickCallback}" style="width: 100%;" class="line-button" variant="default" size="medium" ${this.loading ? "loading" : "not-loading"}>
          <div slot="prefix" class="line-prefix">
            <sl-icon slot="prefix" name="${this.prefixIcon}"></sl-icon>
            <span>${this.linePrefix}</span>
          </div>
          <slot></slot>
          <div slot="suffix" class="line-suffix">
            <sl-tag variant="${this.direction === 'I' ? 'primary' : 'danger'}" size="small" pill>${this.lineSuffix}</sl-tag>
          </div>
        </sl-button>
        <sl-divider></sl-divider>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'tomate-line': TomateLine;
  }
}
