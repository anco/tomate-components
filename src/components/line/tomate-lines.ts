import { LitElement, html, css, PropertyValueMap } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { repeat } from 'lit/directives/repeat.js';
import { TomateLine } from './tomate-line';
import type { Line, LineData, RouteData, StopsData } from '../../types.ts/line';

/**
 * Element that holds a bus line information.
 */
@customElement('tomate-lines')
export class TomateLines extends LitElement {
  static override styles = css`
    .line-items::marker {
      display:none;
    }
  `;

  /**
   * Lines information
   */
  @property()
  lines: Line[] = []

  /**
   * Filtered Lines information
   */
  @state()
  filteredLines: Line[] = []

  /**
   * Filtered Lines information
   */
  @state()
  query: string = ''


  constructor() {
    super();
    this.filteredLines = [...this.lines];
  }

  protected override willUpdate(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): void {
    if (this.filteredLines.length == 0 && !(this.query.trim().toLowerCase())) {
      this.filteredLines = [...this.lines];
    }
  }

  _filter(_query: string) {
    this.query = _query.trim().toLowerCase();
    if (this.query) {
      this.filteredLines = this.lines.filter(line => {
        const fullName = line.lineData.name.replace('R', 'Ramal').replace('T', 'Troncal').replace('D', 'Domingos')
        return line.routeData.name.toLowerCase().includes(this.query) || fullName.toLowerCase().includes(this.query)
      });
    } else {
      this.filteredLines = this.lines
    }
  }

  @property()
  _clickCallback = async (routeData: RouteData, lineData: LineData, stopsData: StopsData) => { }


  override render() {
    return html`
      <div>
      ${repeat(this.filteredLines, (line) => line.routeData.id, (line) => {
      const tomateLine = new TomateLine()
      tomateLine.innerText = line.routeData.name;
      tomateLine.linePrefix = line.lineData.name.replaceAll(' ', '');
      try {
        tomateLine.direction = line.routeData.direction;
        tomateLine.lineSuffix = line.routeData.direction === 'I' ? 'IDA' : 'VUELTA';

        // TODO use global datasources and query functions, pass ids along
        tomateLine.lineData = line.lineData;
        tomateLine.routeName = line.routeData.name;
        tomateLine.routeId = line.routeData.id;
        tomateLine.stopsData = line.stopsData.stops;
        tomateLine.stopsCoordinates = line.stopsData.route;
        tomateLine._clickCallback = async () => {
          // Hack: somehow having a loading propery in TomateLine does not work
          tomateLine.shadowRoot.children[0].children[0].loading = true
          await this._clickCallback(line.routeData, line.lineData, line.stopsData)
          tomateLine.shadowRoot.children[0].children[0].loading = false
        }
      } catch (e) {
        return html`<span>${e}</span>`
      }
      return tomateLine
    })}
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'tomate-lines': TomateLines;
  }
}
