import { LitElement, html, css } from 'lit';
import { customElement, property } from 'lit/decorators.js';

/**
 * Back button for lines and routes
 */
@customElement('tomate-back-button')
export default class TomateBackButton extends LitElement {
  static override styles = css`
    .tomate-back-button-icon-prefix {
      color: tomato;
      font-weight: var(--sl-font-weight-semibold);
      font-family: var(--sl-font-mono);
      font-size: var(--sl-font-size-medium);
    }

    .tomate-back-button-prefix {
      display: flex;
      align-items: center;
    }

    sl-button.tomate-back-button::part(base) {
      border-style: none;
    }
  `

  @property()
  prefixLabel = ''

  override render() {
    return html`
      <sl-button variant="default" size="medium" class="tomate-back-button">
        <div slot="prefix" class="tomate-back-button-prefix">
          <sl-icon name="chevron-left"></sl-icon>
          <span class="tomate-back-button-icon-prefix">${this.prefixLabel}</span>
        </div>
        <slot></slot>
      </sl-button>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'tomate-back-button': TomateBackButton;
  }
}