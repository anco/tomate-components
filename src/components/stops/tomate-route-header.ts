import { LitElement, html, css } from 'lit';
import { customElement } from 'lit/decorators.js';

/**
 * Element that holds a a route name and a change direction button.
 * Used as a title before a list of bus stops.
 */
@customElement('tomate-route-header')
export default class TomateRouteHeader extends LitElement {
  static override styles = css`
    sl-button.change-direction-button::part(label) {
      padding-left: 0;
    }

    .route-name-container {
      align-items: start;
      display: flex;
      flex-direction: column;
    }

    .route-name-container>sl-tooltip>sl-icon {
      color: var(--sl-color-primary-600)
    }

    .route-name-container>sl-tooltip>sl-icon:hover {
      color: var(--sl-color-primary-500)
    }

    .tomate-route-header {
      display: flex;
      flex-direction: column;
      color: var(--sl-color-gray-700);
      align-items: start;
      font-size: var(--sl-font-size-large);
      font-weight: var(--sl-font-weight-bold);
      font-family: var(--sl-font-sans);
      margin-top: var(--sl-spacing-medium);
      padding: var(--sl-spacing-x-small);
      border-style: solid;
      border-left: none;
      border-right: none;
      border-bottom-color: var(--sl-color-gray-300);
      border-top-color: var(--sl-color-gray-300);
      border-width: var(--sl-input-border-width);
    }
  `

  override render() {
    /*
    // TODO: disabling this as there is a bug:
    // and event that the drawer is closed is sent but the drawer does not close
    // find a minimal reproducible case and report it (this is probably due to the carousel)
    const infoTooltip = document.createElement('sl-tooltip');
    infoTooltip.content = 'Hacé click en una parada para ver sus horarios.'
    infoTooltip.placement = 'right'
    infoTooltip.hoist = true;
    infoTooltip.appendChild(infoIcon);
    */

    // TODO: disabling the change direction button until my datasources are
    // better prepared to deal with that use case.
    // TODO: this below could be a dropdown with a menu
    /*
    <sl-button class="change-direction-button" variant="text" size="small" caret>
      Cambiar Sentido
    </sl-button>
    */
    return html`
      <div class="tomate-route-header">
        <div class="route-name-container">
          <sl-icon name="info-circle" label="Información sobre paradas." style="display: none;"></sl-icon>
          <slot></slot>
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'tomate-route-header': TomateRouteHeader;
  }
}