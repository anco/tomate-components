export type RouteData = {
  id: string
  name: string
  direction: string
  [key: string]: any // others
}

export type Stop = {
  lat: string, lon: string, stop_name: string, name: string, line: string, code: string
}

export type StopsData = {
  stops: Stop[]
  route: number[][]
  [key: string]: any // others
}

export type LineData = {
  name: string
  [key: string]: any // others
}

export type Line = {
  routeData: RouteData
  lineData: LineData
  stopsData: StopsData
}

