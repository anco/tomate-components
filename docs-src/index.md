---
layout: page.11ty.cjs
title: <tomate-line> ⌲ Home
---

# &lt;tomate-line>

`<tomate-line>` is an awesome element. It's a great introduction to building web components with LitElement, with nice documentation site as well.

## As easy as HTML

<section class="columns">
  <div>

`<tomate-line>` is just an HTML element. You can it anywhere you can use HTML!

```html
<tomate-line></tomate-line>
```

  </div>
  <div>

<tomate-line></tomate-line>

  </div>
</section>

## Configure with attributes

<section class="columns">
  <div>

`<tomate-line>` can be configured with attributed in plain HTML.

```html
<tomate-line name="HTML"></tomate-line>
```

  </div>
  <div>

<tomate-line name="HTML"></tomate-line>

  </div>
</section>

## Declarative rendering

<section class="columns">
  <div>

`<tomate-line>` can be used with declarative rendering libraries like Angular, React, Vue, and lit-html

```js
import {html, render} from 'lit-html';

const name = 'lit-html';

render(
  html`
    <h2>This is a &lt;tomate-line&gt;</h2>
    <tomate-line .name=${name}></tomate-line>
  `,
  document.body
);
```

  </div>
  <div>

<h2>This is a &lt;tomate-line&gt;</h2>
<tomate-line name="lit-html"></tomate-line>

  </div>
</section>
