---
layout: example.11ty.cjs
title: <tomate-line> ⌲ Examples ⌲ Basic
tags: example
name: Basic
description: A basic example
---

<style>
  tomate-line p {
    border: solid 1px blue;
    padding: 8px;
  }
</style>
<tomate-line>
  <p>This is child content</p>
</tomate-line>

<h3>CSS</h3>

```css
p {
  border: solid 1px blue;
  padding: 8px;
}
```

<h3>HTML</h3>

```html
<tomate-line>
  <p>This is child content</p>
</tomate-line>
```
