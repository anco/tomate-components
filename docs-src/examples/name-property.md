---
layout: example.11ty.cjs
title: <tomate-line> ⌲ Examples ⌲ Name Property
tags: example
name: Name Property
description: Setting the name property
---

<tomate-line name="Earth"></tomate-line>

<h3>HTML</h3>

```html
<tomate-line name="Earth"></tomate-line>
```
